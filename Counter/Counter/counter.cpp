// Includes the complimenting header file.
#include "Counter.h"

// The value variable is an integer responsible for storing the value of the counter.
int value;

// The constructor of the class. This runs when an new instance of the class is created.
Counter::Counter() {
	reset();
}

// The reset() method resets the value of the counter to 0.
void Counter::reset() {
	value = 0;
}

// The increment() method adds 1 to the _value variable.
void Counter::increment() {
	value++;
}

// The print() method prints the current value of the variable to the console.
void Counter::print() {
	cout << value << endl;
}

// Returns the value variable.
int Counter::get_value() {
	return value;
}