// Include iostream to allow us to use cout and endl to print to the console.
#include <iostream>

// Use the std namespace, for cout and endl.
using namespace std;

// The Counter class. This class contains a variable that holds a value, and methods for manipulating and accessing that value. For method and variable descriptions, please see Counter.cpp.
class Counter {
// All the following methods are public.
public:
	Counter();
	void reset();
	void increment();
	void print();
	int get_value();
};