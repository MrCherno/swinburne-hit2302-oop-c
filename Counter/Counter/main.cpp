// Include the Counter class' header file so we can use it in this file.
#include "Counter.h"

// The main() function which is the entry point to our program.
int main() {
	// Create a instance of our Counter class.
	Counter* counter = new Counter();
	// Print the current value of the counter to the console.
	counter->print();
	// Increment the value 10 times.
	for (int i = 0; i < 10; i++) {
		counter->increment();
	}
	// Print the new value to the console.
	counter->print();
	// Reset the value of the counter.
	counter->reset();
	// Print the new value to the console.
	counter->print();
	// Delete the counter object and release it from memory.
	delete counter;
	// Don't close the console until the user has pressed enter.
	cin.ignore();
	// Return with a normal exit status (0).
	return 0;
}