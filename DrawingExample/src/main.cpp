// Includes the SwinGame library.
#include "SwinGame.h"
// Includes our Shape class.
#include "shape.h"
// Includes iostream to gain access to cout.
#include <iostream>

// Uses the std namespace (to avoid having to type std::cout, for example).
using namespace std;

int main() {
	// Sets format of booleans to print "true" instead of 1 and "false" instead of 0 to the console.
	cout.setf(ios::boolalpha);

	// Initilizes SwinGame and creates a window.
    open_audio();
    open_graphics_window("Shapes!", 800, 600);
    load_default_colors();
    show_swin_game_splash_screen();

	// Creates a new instance of the Shape object with the specified parameters.
    Shape* shape = new Shape(80, 80, 500, 400);
	// Sets the colour of the rectangle to turquoise.
	shape->set_color(ColorTurquoise);
	
	// Creates a point2d that is inside the rectangle's bounds.
	point2d inside;
	inside.x = 120;
	inside.y = 300;

	// Prints to the console whether the specified point (the inside point2d variable we created) is inside the rectangle or not.
	cout << shape->is_at(inside) << endl;

	// The main game loop.
	while (!window_close_requested()) {
		// Processes events, such as the close button being pressed on the window.
		process_events();
		// Clears the screen to white.
        clear_screen(ColorWhite);
		// Renders the rectangle to the screen.
		shape->render();
		// Refreshes (swaps buffers) the screen to show the rectangle.
        refresh_screen();
	}
	// Destructs the shape object and removes it from memory.
    delete shape;
	// Closes the SwinGame audio.
    close_audio();
	// Releases all resources used by the program.
    release_all_resources();
	// Exits with a status of 0 (everything is fine).
    return 0;
}
