// Includes the header file for this class.
#include "shape.h"

// Stores the colour of the rectangle.
color _color;
// Stores the position of the as a point2d type.
point2d _position;
// These two integers store the width and height of the rectangle, respectively.
int _width, _height;

// Constructor: this is executed every time a new instance of the Shape class is created. The x, y, width, and height of the rectangle are specified here.
Shape::Shape(int x, int y, int width, int height) {
	_position.x = x;
	_position.y = y;
	_width = width;
	_height = height;
}

// The rectangle's width is set to the provided width in the parameter.
void Shape::set_width(int width) {
	_width = width;
}
// Returns the rectangle's width.
int Shape::get_width() {
	return _width;
}
// The rectangle's height is set to the provided height in the parameter.
void Shape::set_height(int height) {
	_height = height;
}
// Returns the rectangle's height.
int Shape::get_height(){
	return _height;
}
// The rectangle's colour is set to the provided colour in the parameter.
void Shape::set_color(color col) {
	_color = col;
}
// Returns the colour of the rectangle.
color Shape::get_color() {
	return _color;
}
// The rectangle's position is set to the provided point2d position in the parameter.
void Shape::set_position(point2d position) {
	_position = position;
}
// Returns the position of the rectangle as a point2d type.
point2d Shape::get_position() {
	return _position;
}
// Returns true if the specified point in the parameter is inside the rectangle's bounds.
bool Shape::is_at(point2d point) {
	if (point.x > _position.x && point.x < _position.x + _width) {
		if (point.y > _position.y && point.y < _position.y + _height) {
			return true;
		}
	}
	return false;
}
// Renders (draws) the rectangle to the screen.
void Shape::render() {
	draw_rectangle(_color, true, _position.x, _position.y, _width, _height);
}
