// Include SwinGame's header file to provide access to SwinGame functions.
#include "SwinGame.h"
// Only include this header file once (bypass the file if it's already included to avoid conflicts).
#pragma once
//
// Shape class:
// The Shape class contains all functions necessary to draw a rectangle of specified colour, size, and location to the screen.
// A isAt() function is also included to check whether a the rectangle encompasses a given point. For field and method descriptions,
// please see the shape.cpp file.
//
class Shape {
public:
	Shape(int x, int y, int width, int height);
	void set_width(int width);
	int get_width();
	void set_height(int height);
	int get_height();
	void set_color(color col);
	color get_color();
	void set_position(point2d position);
	point2d get_position();
	bool is_at(point2d point);
	void render();
};

