// Includes our relative header file.
#include "die.h"

// The Die constructor sets the sides variable equal to the parameter.
Die::Die(int sides) {
	_sides = sides;
}

// "Rolls" the die. A random number between 1 and the specified number of sides is generated and assigned to the _top_value variable.
void Die::roll() {
	_top_value = rand() % _sides + 1;
}

// The current value of the die is returned.
int Die::get_top_value() {
	return _top_value;
}

// The number of sides on this die is returned.
int Die::get_sides() {
	return _sides;
}
