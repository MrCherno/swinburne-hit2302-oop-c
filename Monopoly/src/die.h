// Only include this file once (if it's already included, skip it)
#pragma once
// Include iostream to let us print to the console via cout.
#include <iostream>
// stdlib is included for the rand() function.
#include <stdlib.h>
// time is included for the time() function.
#include <time.h>

// The Die class stores a die with a specified number of sides, and allows it to be rolled, as well as its properties returned.
class Die {
public:
	Die(int sides);
	void roll();
	int get_top_value();
	int get_sides();
private:
	int _top_value, _sides, seed;
};

