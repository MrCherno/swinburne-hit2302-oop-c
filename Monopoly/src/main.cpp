// Inlcuding our Die class in this file.
#include "die.h"
// Including iostream to allow us to read and write to the console.
#include <iostream>

// Using the std namespace for cout and cin functions.
using namespace std;

// The entry point of our program.
int main() {
	// Set the seed for the random number generator.
	srand(time(NULL));

	// A local variable used for storing the number of sides on the dice.
	int sides0, sides1;
	
	// A nice, friendly message to tell the user to enter the number of sides on their dice.
	cout << "Please enter the number of sides on first dice: ";

	// Store whatever number the user input into the console, in the sides variable. 
	cin >> sides0;

	// Create the first Die object with the specified number of sides.
	Die* die0 = new Die(sides0);

	// A nice, friendly message to tell the user to enter the number of sides on their dice.
	cout << "Please enter the number of sides on second dice: ";

	// Store whatever number the user input into the console, in the sides variable. 
	cin >> sides1;

	// Create the second Die object with the specified number of sides.
	Die* die1 = new Die(sides1);

	// Create and set running equal to true to start the while loop. This will keep the while asking for user input until it is false.
	bool running = true;

	// The main loop. As long as running is true, the user will be asked to select an option from the menu. Based on the selection, the program will
	// then either quit, roll the dice, or show the user the values on the dice.
	while (running) {
		cout << "\nWould you like to:\n[R]oll the dice?\n[L]ook at Dice\n[Q]uit" << endl;
		char choice;
		cin >> choice;
		if (choice == 'r' || choice == 'R') {
			die0->roll();
			die1->roll();
		}
		if (choice == 'l' || choice == 'L') {
			cout << die0->get_top_value() << endl;
			cout << die1->get_top_value() << endl;
		}
		if (choice == 'q' || choice == 'Q') running = false;
	}

	// Remove both Die objects from memory since they're not needed anymore.
	delete die0;
	delete die1;

	// Return status 0, everything is okay. :)
	return 0;
}